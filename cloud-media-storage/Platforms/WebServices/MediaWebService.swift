//
//  MediaWebService.swift
//  cloud-media-storage
//
//  Created by William Quach on 15/07/2022.
//

import Foundation
import UIKit

class MediaWebService: MediaService {

    public static let shared: MediaService = MediaWebService();

    private var SCHEME: String {
        ProcessInfo.processInfo.environment["API_SCHEME"]!
    }
    private var HOST: String {
        ProcessInfo.processInfo.environment["API_HOST"]!;
    }
    private var PORT: Int {
        Int(ProcessInfo.processInfo.environment["API_PORT"]!)!;
    }

    private var PATH = "/image";

    private var url: URL? = nil;

    // Constructor
    private init() {
        var components = URLComponents();
        components.scheme = SCHEME
        components.host = HOST
        components.port = PORT
        components.path = PATH
        url = components.url;
    }


    func getUserMedias(completion: @escaping ([Media]) -> Void) {
        print("Getting user medias")

        guard let url = url else {
            print("Invalid URL")
            completion([]);
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "GET"

        let dataTask = URLSession.shared.dataTask(with: request) { data, response, err in
            if err != nil {
                print("== Error when getting medias ==")
                print(err!);
                completion([]);
                return;
            }

            if let response = response as? HTTPURLResponse {
                if response.statusCode == 200 {
                    guard let data = data,
                          let json = try? JSONSerialization.jsonObject(with: data, options: .fragmentsAllowed) as? [[String: Any]]
                    else {
                        completion([])
                        return
                    }

                    let medias = json.compactMap({ dict in
                        Media(dict: dict);
                    });
                    completion(medias);
                } else {
                    print("Status code is not 200");
                    print(response);
                    completion([]);
                }
                return;
            }
            print("Response is not HTTPURLResponse");
            print(response!);
            completion([]);
            return;
        }
        dataTask.resume();
    }

    func uploadMedia(fileName: String, image: UIImage, completion: @escaping (UploadMediaResponse) -> ()) {
        print("Uploading image : \(fileName)...");

        guard let url = url else {
            print("Invalid URL")
            completion(UploadMediaResponse(success: false, message: "Invalid URL"));
            return
        }

        // generate boundary string using a unique per-app string
        let boundary = UUID().uuidString;
        let session = URLSession.shared;

        // Set the URLRequest to POST and to the specified URL
        var urlRequest = URLRequest(url: url);
        urlRequest.httpMethod = "POST"

        // Set Content-Type Header to multipart/form-data, this is equivalent to submitting form data with file upload in a web browser
        // And the boundary is also set here
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        var data = Data()

        // Add the image data to the raw http request data
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"1\"; filename=\"\(fileName)\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        // Create png data from jpegData
        data.append(image.jpegData(compressionQuality: 1.0)!)
        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)

        // Send a POST request to the URL, with the data we created earlier
        session.uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, error in
                    if error == nil {
                        if let response = response as? HTTPURLResponse {
                            if response.statusCode == 200 {
                                if let responseString = String(data: responseData!, encoding: .utf8) {
                                    print("Response string : \(responseString)");
                                    completion(UploadMediaResponse(success: true, message: NSLocalizedString("medias.upload.message.success", comment: ""), url: responseString))
                                    return;
                                } else {
                                    print("Failed to parse upload image response in string!");
                                    completion(UploadMediaResponse(success: false, message: NSLocalizedString("medias.upload.message.error", comment: "")));
                                    return;
                                }
                            } else {
                                print("Failed to upload image : \(fileName)!");
                                completion(UploadMediaResponse(success: false, message: NSLocalizedString("medias.upload.message.error", comment: "")));
                                return;
                            }
                        }
                    }
                    print("Failed to upload image : \(fileName)!");

                    completion(UploadMediaResponse(success: false, message: NSLocalizedString("medias.upload.message.error", comment: "")));
                })
                .resume()
    }

    func updateMedia(id: Int, fileName: String, image: UIImage, completion: @escaping (UpdateMediaResponse) -> Void) {
        print("Updating image : '\(id)'...");
        print("With file name : '\(fileName)'...");

        var components = URLComponents()
        components.scheme = SCHEME
        components.host = HOST
        components.port = PORT
        components.path = PATH + "/\(String(id))"

        guard let url = components.url else {
            print("Invalid URL")
            completion(UpdateMediaResponse(success: false, message: "Invalid URL"));
            return
        }

        // generate boundary string using a unique per-app string
        let boundary = UUID().uuidString;
        let session = URLSession.shared;

        // Set the URLRequest to POST and to the specified URL
        var urlRequest = URLRequest(url: url);
        urlRequest.httpMethod = "PUT"

        // Set Content-Type Header to multipart/form-data, this is equivalent to submitting form data with file upload in a web browser
        // And the boundary is also set here
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        var data = Data()

        // Add the image data to the raw http request data
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"1\"; filename=\"\(fileName)\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        data.append(image.pngData()!)
        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)

        // Send a POST request to the URL, with the data we created earlier
        session.uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, error in
                    if error == nil {
                        if let response = response as? HTTPURLResponse {
                            if response.statusCode == 200 {
                                if let responseString = String(data: responseData!, encoding: .utf8) {
                                    print("Response string : \(responseString)");
                                    completion(UpdateMediaResponse(success: true, message: NSLocalizedString("medias.upload.message.success", comment: ""), url: responseString))
                                    return;
                                } else {
                                    print("Failed to parse upload image response in string!");
                                    completion(UpdateMediaResponse(success: false, message: NSLocalizedString("medias.upload.message.error", comment: "")));
                                    return;
                                }
                            } else {
                                print("Failed to upload image : \(fileName)!");
                                completion(UpdateMediaResponse(success: false, message: NSLocalizedString("medias.upload.message.error", comment: "")));
                                return;
                            }
                        }
                    }
                    print("Failed to upload image : \(fileName)!");

                    completion(UpdateMediaResponse(success: false, message: NSLocalizedString("medias.upload.message.error", comment: "")));
                })
                .resume()
    }

    func deleteMedia(media: Media!, completion: @escaping (DeleteMediaResponse) -> Void) {
        print("Deleting image : '\(media.id)'...");
        print("With file name : '\(media.name)'...");

        var components = URLComponents()
        components.scheme = SCHEME
        components.host = HOST
        components.port = PORT
        components.path = PATH + "/\(String(media.id))"

        guard let url = components.url else {
            print("Invalid URL")
            completion(DeleteMediaResponse(success: false, message: "Invalid URL"));
            return
        }

        let session = URLSession.shared;

        // Set the URLRequest to DELETE and to the specified URL
        var urlRequest = URLRequest(url: url);
        urlRequest.httpMethod = "DELETE"

        // Send a DELETE request to the URL
        session.dataTask(with: urlRequest, completionHandler: { data, response, error in
                    if error == nil {
                        if let response = response as? HTTPURLResponse {
                            if response.statusCode == 200 {
                                print("Image deleted : '\(media.name)', status code : \(response.statusCode)");
                                if let responseString = String(data: data!, encoding: .utf8) {
                                    if responseString == "true" {
                                        print("Response string : \(responseString)");
                                        completion(DeleteMediaResponse(success: true, message: NSLocalizedString("medias.delete.message.success", comment: ""), url: responseString))
                                        return;
                                    } else {
                                        print("Delete media response is false!");
                                        completion(DeleteMediaResponse(success: false, message: NSLocalizedString("medias.delete.message.error", comment: "")));
                                        return;
                                    }
                                } else {
                                    print("Failed to parse delete image response in string!");
                                    completion(DeleteMediaResponse(success: false, message: NSLocalizedString("medias.delete.message.error", comment: "")));
                                    return;
                                }
                            } else {
                                print("Failed to delete image : \(media.name)!");
                                completion(DeleteMediaResponse(success: false, message: NSLocalizedString("medias.delete.message.error", comment: "")))
                                return;
                            }
                        }
                    }
                    completion(DeleteMediaResponse(success: false, message: NSLocalizedString("medias.delete.message.error", comment: "")));
                })
                .resume()
    }
}
