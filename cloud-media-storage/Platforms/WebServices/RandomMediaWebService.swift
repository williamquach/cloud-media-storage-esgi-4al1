//
//  MediaWebService.swift
//  cloud-media-storage
//
//  Created by William Quach on 15/07/2022.
//

import Foundation
import UIKit

class RandomMediaWebService: RandomMediaService {

    public static let shared: RandomMediaService = RandomMediaWebService();

    private var SCHEME: String {
        ProcessInfo.processInfo.environment["RANDOM_MEDIA_API_SCHEME"]!
    }
    private var HOST: String {
        ProcessInfo.processInfo.environment["RANDOM_MEDIA_API_HOST"]!;
    }
    private var PORT: Int {
        Int(ProcessInfo.processInfo.environment["RANDOM_MEDIA_API_PORT"]!)!;
    }

    private var SQUARE_PATH = "/image";
    private var RECTANGLE_PATH = "/image";

    private var square_media_url: URL? = nil;
    private var rectangle_media_url: URL? = nil;

    // Constructor
    private init() {
        var squareMediaComponents = URLComponents();
        squareMediaComponents.scheme = SCHEME
        squareMediaComponents.host = HOST
        squareMediaComponents.port = PORT
        squareMediaComponents.path = SQUARE_PATH
        squareMediaComponents.queryItems = [
            URLQueryItem(name: "w", value: "400"),
            URLQueryItem(name: "h", value: "400")
        ]

        square_media_url = squareMediaComponents.url;

        var rectangleMediaComponents = URLComponents();
        rectangleMediaComponents.scheme = SCHEME
        rectangleMediaComponents.host = HOST
        rectangleMediaComponents.port = PORT
        rectangleMediaComponents.path = RECTANGLE_PATH
        rectangleMediaComponents.queryItems = [
            URLQueryItem(name: "w", value: "400"),
            URLQueryItem(name: "h", value: "600")
        ]
        rectangle_media_url = rectangleMediaComponents.url;
    }

    func getSquareRandomMedia(completion: @escaping (Media?) -> ()) {
        guard let square_media_url = square_media_url else {
            print("Error: square_media_url is nil");
            return;
        }

        let task = URLSession.shared.dataTask(with: square_media_url) { (data, response, error) in
            if let error = error {
                print("Error when getting square random media: \(error)");
                return;
            }

            if let response = response as? HTTPURLResponse {
                if response.statusCode == 200 {
                    print("Response: \(response)");
                    guard let imageUrl = response.url?.absoluteString as? String else {
                        print("Error: response url is nil");
                        return;
                    }
                    let imageId = Date().timeIntervalSince1970;
                    let imageName = imageUrl.components(separatedBy: "/").last;
                    let media = Media(id: Int(imageId), name: String(imageName!), link: imageUrl, linkURL: URL(string: imageUrl)!);
                    completion(media);
                } else {
                    print("Error: response status code is not 200");
                    completion(nil);
                }
            } else {
                print("Error: response is not HTTPURLResponse");
                completion(nil);
            }
        }
        task.resume();
    }

    func getRectangleRandomMedia(completion: @escaping (Media?) -> ()) {
        guard let rectangle_media_url = rectangle_media_url else {
            print("Error: rectangle_media_url is nil");
            return;
        }
        let task = URLSession.shared.dataTask(with: rectangle_media_url) { (data, response, error) in
            if let error = error {
                print("Error: \(error)");
                return;
            }

            if let response = response as? HTTPURLResponse {
                if response.statusCode == 200 {

                }
            }

            guard let data = data else {
                print("Error: data is nil");
                return;
            }
            print("Data: \(data)");
            let media = Media(id: 1, name: "rectangle_media", link: "", linkURL: URL(string: "")!);
            completion(media);
        }
        task.resume();
    }
}
