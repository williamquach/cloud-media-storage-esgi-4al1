//
//  UserAPIWebService.swift
//  cloud-media-storage
//
//  Created by William Quach on 11/07/2022.
//

import Foundation

class UserWebService: UserService {

    public static let shared: UserService = UserWebService();

    private var SCHEME: String {
        ProcessInfo.processInfo.environment["API_SCHEME"]!
    }
    private var HOST: String {
        ProcessInfo.processInfo.environment["API_HOST"]!;
    }
    private var PORT: Int {
        Int(ProcessInfo.processInfo.environment["API_PORT"]!)!;
    }

    private var LOGIN_PATH = "/login";
    private var SIGN_UP_PATH = "/users/register";

    func login(nickname: String, password: String, completion: @escaping (UserLoginResponse) -> Void) {
        var components = URLComponents()
        components.scheme = SCHEME;
        components.host = HOST;
        components.port = PORT;
        components.path = LOGIN_PATH;

        // MARK: Create URL
        guard let url = components.url else {
            print("Invalid URL")
            completion(UserLoginResponse(success: false, message: NSLocalizedString("application.error", comment: "")));
            return
        }

        // MARK: URLRequest
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let parameterDictionary = ["username": nickname, "password": password]
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
            return;
        }
        request.httpBody = httpBody;

        let dataTask = URLSession.shared.dataTask(with: request) { data, response, err in
            if err != nil {
                print("Got an error while login : ");
                print(err as Any);

                completion(UserLoginResponse(success: false, message: NSLocalizedString("login.incorrect.login.message", comment: "")));
                return;
            }

            if let response = response as? HTTPURLResponse {
                if response.statusCode == 200 {
                    completion(UserLoginResponse(success: true, message: NSLocalizedString("login.success.title", comment: "")));
                } else {
                    completion(UserLoginResponse(success: false, message: NSLocalizedString("login.incorrect.login.message", comment: "")));
                }

                return;
            }
            completion(UserLoginResponse(success: false, message: NSLocalizedString("what.happened", comment: "")));
            return;
        }
        dataTask.resume();
    }

    func register(nickname: String,
                  password: String,
                  email: String,
                  firstname: String,
                  lastname: String,
                  completion: @escaping (UserRegisterResponse) -> Void) -> Void {

        var components = URLComponents()
        components.scheme = SCHEME;
        components.host = HOST;
        components.port = PORT;
        components.path = SIGN_UP_PATH;

        // MARK: Create URL
        guard let url = components.url else {
            print("Invalid URL")
            completion(UserRegisterResponse(success: false, message: NSLocalizedString("application.error", comment: "")));
            return
        }

        // MARK: URLRequest
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let parameterDictionary = [
            "username": nickname,
            "password": password,
            "email": email,
            "firstname": firstname,
            "lastname": lastname
        ]
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
            return;
        }
        request.httpBody = httpBody;

        let dataTask = URLSession.shared.dataTask(with: request) { data, response, err in
            if err != nil {
                completion(UserRegisterResponse(success: false, message: NSLocalizedString("sign_up.incorrect.register.message", comment: "")));
                return;
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    completion(UserRegisterResponse(success: true, message: NSLocalizedString("sign_up.success.title", comment: "")));
                } else {
                    completion(UserRegisterResponse(success: false, message: NSLocalizedString("sign_up.incorrect.register.message", comment: "")));
                }

                return;
            }
            completion(UserRegisterResponse(success: false, message: NSLocalizedString("what.happened.during.upload", comment: "")));
            return;
        }
        dataTask.resume();

    }
}
