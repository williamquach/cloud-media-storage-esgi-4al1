//
// Created by William Quach on 23/07/2022.
//

import Foundation
import UIKit

extension ImageCache {
    func image(for url: URL) -> UIImage? {
        lock.lock(); defer {
            lock.unlock()
        }
        // the best case scenario -> there is a decoded image
        if let decodedImage = decodedImageCache.object(forKey: url as AnyObject) as? UIImage {
            print("Found decoded image in cache!");
            return decodedImage
        }
        // search for image data
        if let image = imageCache.object(forKey: url as AnyObject) as? UIImage {
            print("Found image data in cache!");
            let decodedImage = image.decodedImage()
            decodedImageCache.setObject(image as AnyObject, forKey: url as AnyObject, cost: /*Get image disk size */ 0)
            return decodedImage
        }
        return nil
    }

    // Get all images from cache
    func images() -> [UIImage] {
        lock.lock(); defer {
            lock.unlock()
        }
        var images = [UIImage]()

        if let allDecodedImagesInCache = decodedImageCache.value(forKey: "allObjects") as? NSArray {
            for decodedImage in allDecodedImagesInCache {
                print("object is \(decodedImage)")
                if let decodedImage = decodedImage as? UIImage {
                    print("Found decoded image in cache when getting all images in cache!");
                    images.append(decodedImage)
                }
            }
        }

        return images;
//        if let image = imageCache.object(forKey: url as AnyObject) as? UIImage {
//            let decodedImage = image.decodedImage()
//            decodedImageCache.setObject(image as AnyObject, forKey: url as AnyObject, cost: /*Get image disk size */ 0)
//            return decodedImage
//        }
//        for (_, value) in decodedImageCache.object() {
//            if let image = value as? UIImage {
//                images.append(image)
//            }
//        }
//        return images
    }
}
