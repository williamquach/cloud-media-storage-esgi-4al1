//
// Created by William Quach on 23/07/2022.
//

import Foundation
import UIKit

extension ImageCache: ImageCacheType {
    func insertImage(_ image: UIImage?, for url: URL) {
        guard let image = image else {
            return removeImage(for: url)
        }
        let decodedImage = image.decodedImage()

        lock.lock(); defer {
            lock.unlock()
        }
        imageCache.setObject(decodedImage, forKey: url as AnyObject)
        decodedImageCache.setObject(image as AnyObject, forKey: url as AnyObject, cost: 0)
    }

    func removeImage(for url: URL) {
        lock.lock(); defer {
            lock.unlock()
        }
        imageCache.removeObject(forKey: url as AnyObject)
        decodedImageCache.removeObject(forKey: url as AnyObject)
    }
    
    func removeAllImages() {
        lock.lock(); defer {
            lock.unlock()
        }
        imageCache.removeAllObjects()
        decodedImageCache.removeAllObjects()
    }
}
