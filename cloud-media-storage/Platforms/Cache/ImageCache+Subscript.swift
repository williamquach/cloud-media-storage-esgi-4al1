//
// Created by William Quach on 23/07/2022.
//

import Foundation
import UIKit

extension ImageCache {
    subscript(_ key: URL) -> UIImage? {
        get {
            return image(for: key)
        }
        set {
            return insertImage(newValue, for: key)
        }
    }
}
