//
//  UserLoginResponse.swift
//  cloud-media-storage
//
//  Created by William Quach on 12/07/2022.
//

import Foundation

class UserLoginResponse: CustomStringConvertible {
    
    let success: Bool;
    let message: String;
    
    var description: String {
        return "success: \(self.success), message: \(self.message)";
    }
    
    internal init(success: Bool, message: String) {
        self.success = success
        self.message = message
    }
    
}
