//
//  UploadMediaResponse.swift
//  cloud-media-storage
//
//  Created by William Quach on 15/07/2022.
//

import Foundation

class UploadMediaResponse: CustomStringConvertible {

    let success: Bool;
    let message: String;
    let url: String;

    var description: String {
        return "success: \(self.success), message: \(self.message), url: \(self.url)";
    }

    internal init(success: Bool, message: String, url: String = "") {
        self.success = success
        self.message = message
        self.url = url
    }
}
