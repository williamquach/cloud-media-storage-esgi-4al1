//
//  UserAccount.swift
//  cloud-media-storage
//
//  Created by William Quach on 11/07/2022.
//

import Foundation

class UserAccount {
    let nickname: String;
    let password: String;

    internal init(nickname: String, password: String) {
        self.nickname = nickname
        self.password = password
    }
}
