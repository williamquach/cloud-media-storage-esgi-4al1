//
//  Image.swift
//  cloud-media-storage
//
//  Created by William Quach on 15/07/2022.
//

import Foundation

class Media: CustomStringConvertible {
    
    let id: Int;
    let name: String;
    let link: String;
    let linkURL: URL;
    
    var description: String {
        return "id: \(self.id), name: \(self.name), link: \(self.link), linkURL: \(self.linkURL)";
    }
    
    init(id: Int, name: String, link: String, linkURL: URL) {
        self.id = id
        self.name = name
        self.link = link
        self.linkURL = linkURL
    }

    convenience init?(dict: [String: Any]) {
        guard let id = dict["id"] as? Int,
              let name = dict["name"] as? String,
              let link = dict["link"] as? String,
              let linkURL = URL(string: link) else {
            return nil;
        }
        self.init(id: id, name: name, link: link, linkURL: linkURL);
    }
}
