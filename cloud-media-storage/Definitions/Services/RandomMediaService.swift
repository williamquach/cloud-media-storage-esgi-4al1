//
//  MediaService.swift
//  cloud-media-storage
//
//  Created by William Quach on 15/07/2022.
//

import Foundation
import UIKit

protocol RandomMediaService {
    func getSquareRandomMedia(completion: @escaping (Media?) -> Void) -> Void;
    func getRectangleRandomMedia(completion: @escaping (Media?) -> Void) -> Void;
}
