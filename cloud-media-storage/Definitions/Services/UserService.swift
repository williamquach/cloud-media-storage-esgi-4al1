//
//  UserAPIService.swift
//  cloud-media-storage
//
//  Created by William Quach on 11/07/2022.
//

import Foundation


// EQ INTERFACE au sens programmation => contrat à respecter
protocol UserService {
    func login(nickname: String, password: String, completion: @escaping (UserLoginResponse) -> Void) -> Void;
    func register(nickname: String,
                  password: String,
                  email: String,
                  firstname: String,
                  lastname: String,
                  completion: @escaping (UserRegisterResponse) -> Void) -> Void;
}
