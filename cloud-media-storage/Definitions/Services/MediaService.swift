//
//  MediaService.swift
//  cloud-media-storage
//
//  Created by William Quach on 15/07/2022.
//

import Foundation
import UIKit

protocol MediaService {
    func getUserMedias(completion: @escaping ([Media]) -> Void) -> Void;
    func uploadMedia(fileName: String, image: UIImage, completion: @escaping (UploadMediaResponse) -> ()) -> Void;
    func updateMedia(id: Int, fileName: String, image: UIImage, completion: @escaping (UpdateMediaResponse) -> Void) -> Void;
    func deleteMedia(media: Media!, completion: @escaping (DeleteMediaResponse) -> Void) -> Void;
}
