//
//  MediaCollectionViewCell.swift
//  cloud-media-storage
//
//  Created by William Quach on 15/07/2022.
//

import UIKit

class MediaCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mediaImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
}
