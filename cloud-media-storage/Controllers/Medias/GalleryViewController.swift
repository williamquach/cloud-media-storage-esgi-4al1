//
//  GaleryViewController.swift
//  cloud-media-storage
//
//  Created by William Quach on 02/07/2022.
//

import UIKit
import Photos

class GalleryViewController: UIViewController, UICollectionViewDelegate, UINavigationControllerDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate {

    @IBOutlet var mediasCollectionView: UICollectionView!
    @IBOutlet weak var mediasLoader: UIActivityIndicatorView!
    @IBOutlet weak var noMediasLabel: UILabel!

    var timer = Timer()

    let cellIdentifier = "USER_MEDIA";
    var mediasInUIImage: [UIImage] = []
    var medias: [Media] = [] {
        didSet {
            DispatchQueue.main.async {
                if self.medias.count <= 0 {
                    self.noMediasLabel.isHidden = false
                    self.noMediasLabel.text = NSLocalizedString("gallery.no_medias", comment: "")
                    self.mediasCollectionView.isHidden = true
                    self.mediasLoader.stopAnimating()
                } else {
                    self.noMediasLabel.isHidden = true
                    self.mediasCollectionView.isHidden = false

                    print("RELOADING COLLECTION VIEW...")
                    self.mediasLoader.startAnimating();
                    self.mediasCollectionView.reloadData();
                    self.mediasLoader.stopAnimating();
                }
            }
        }
    };

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("gallery.page.title", comment: "");

        let reloadMediasButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(reloadMedias))
        self.navigationItem.rightBarButtonItem = reloadMediasButton;
        // Disconnect button barButtonSystemItem: , target: self, action: #selector(disconnect)
        let disconnectButton = UIBarButtonItem(title: NSLocalizedString("gallery.disconnect", comment: ""), style: .plain, target: self, action: #selector(disconnect))
        self.navigationItem.leftBarButtonItem = disconnectButton;

        let cellNib = UINib(nibName: "MediaCollectionViewCell", bundle: nil);

        self.mediasCollectionView.register(cellNib, forCellWithReuseIdentifier: cellIdentifier)
        self.mediasCollectionView.delegate = self;
        self.mediasCollectionView.dataSource = self;
        self.retrieveUserMedias();

        // Launch timer to upload medias in cache every 10 seconds
        timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(uploadCachedMedias), userInfo: nil, repeats: true)
    }

    @objc func disconnect() {
        self.navigationController?.popViewController(animated: true);
    }

    @objc func reloadMedias() {
        self.retrieveUserMedias();
    }

    func retrieveUserMedias() {
        self.mediasLoader.startAnimating()
        if Reachability.isConnectedToNetwork() {
            MediaWebService.shared.getUserMedias { mediasRetrieved in
                print("RETRIEVED MEDIAS: ");
                print(mediasRetrieved);
                self.medias = mediasRetrieved
            }
        } else {
            self.mediasLoader.stopAnimating()
            // Alert no connection available, user can only upload medias to cache storage
            let alert = UIAlertController(title: NSLocalizedString("no.internet.title", comment: ""), message: NSLocalizedString("no.internet.functionalities.available", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        medias.count;
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let imageData = try? Data(contentsOf: medias[indexPath.row].linkURL),
              let loadedImage = UIImage(data: imageData)
        else {
            return UICollectionViewCell();
        }
        mediasInUIImage.append(loadedImage);
        mediasInUIImage[indexPath.row] = loadedImage;

        print("LOADING IMAGE: " + medias[indexPath.row].linkURL.absoluteString);

        if let reuseCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? MediaCollectionViewCell {
            reuseCell.mediaImageView.image = loadedImage
            return reuseCell;
        }

        let cell = MediaCollectionViewCell();
        cell.mediaImageView.image = loadedImage
        return cell;
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let mediaDetailsController = MediaDetailsViewController.newInstance(media: medias[indexPath.row], mediaUIImage: mediasInUIImage[indexPath.row])
        mediaDetailsController.delegate = self;
        self.navigationController?.pushViewController(mediaDetailsController, animated: true);
    }

    @IBAction func addMediaFromGallery() {
        guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
            return;
        }
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary;
        picker.allowsEditing = true;
        picker.delegate = self;
        self.present(picker, animated: true);
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        mediasLoader.startAnimating()
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            picker.dismiss(animated: true);

            if let asset = info[UIImagePickerController.InfoKey.imageURL] as? URL {
                let fileName = asset.lastPathComponent
                uploadMedia(fileName: fileName, image: image);
            } else {
                print("Uploading image taken from camera");
                let correctOrientationImage = UIImage(cgImage: image.cgImage!, scale: 1.0, orientation: .up);
                print(correctOrientationImage)
                print(correctOrientationImage.description)
                // Generate media file name from date
                let fileName: String = String(format: "%.0f", Date().timeIntervalSince1970) + ".jpg";
                uploadMedia(fileName: fileName, image: correctOrientationImage);
            }
        } else {
            print("No image selected");
            mediasLoader.stopAnimating();
        }
    }

    private let cache = ImageCache()

    func uploadMedia(fileName: String, image: UIImage) -> Bool {
        mediasLoader.startAnimating()

        if Reachability.isConnectedToNetwork() {
            var successUpload = false;
            MediaWebService.shared.uploadMedia(fileName: fileName, image: image) { response in
                print("Finished uploading image");
                print(response);
                if response.success == true {
                    print("Successfully uploaded image");
                    // Display toast message
                    self.medias.append(Media(id: 0, name: fileName, link: response.url, linkURL: URL(string: response.url)!));
                    DispatchQueue.main.async {
//                        self.retrieveUserMedias();
                        self.showToast(message: response.message, font: .systemFont(ofSize: 16))
                        self.mediasLoader.stopAnimating()
                    }
                    successUpload = true;
                } else {
                    print("Failed to upload image");
                    DispatchQueue.main.async {
                        self.showToast(message: response.message, font: .systemFont(ofSize: 16))
                        self.mediasLoader.stopAnimating()
                    }
                    successUpload = false;
                }
            }
            return successUpload
        } else {
            print("NO INTERNET CONNECTION");
            DispatchQueue.main.async {
                self.mediasLoader.stopAnimating();
                let alert = UIAlertController(title: NSLocalizedString("no.internet.title", comment: ""), message: NSLocalizedString("no.internet.functionalities.available", comment: ""), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: nil))
                self.present(alert, animated: true)
            }
            let url = URL(string: String(image.hash))!;
            cache.insertImage(image, for: url);
            return false;
        }
    }

    @objc func uploadCachedMedias() {
        if Reachability.isConnectedToNetwork() {
            let medias = cache.images();
            for media in medias {
                print("Uploading cached media: " + String(media.hash));
                let mediaHashUsedInCache = String(media.hash);
                uploadCachedMediaThenRemove(fileName: mediaHashUsedInCache, image: media)
            }
        }
    }

    func uploadCachedMediaThenRemove(fileName: String, image: UIImage) {
        mediasLoader.startAnimating()
        MediaWebService.shared.uploadMedia(fileName: fileName, image: image) { response in
            print("Finished uploading image from cache");
            print(response);
            if response.success == true {
                print("Successfully uploaded image from cache");
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: NSLocalizedString("medias.upload.message.success", comment: ""), message: NSLocalizedString("medias.upload.alert.success.from.cache", comment: ""), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: nil))
                    self.present(alert, animated: true)
                }

                guard let url = URL(string: fileName) else {
                    print("Failed to create URL from : " + fileName);
                    return;
                }
                print("Deleting cached media: " + fileName);
                self.cache.removeImage(for: url);
                print("Retrieving deleted media: ");
                print(self.cache.image(for: url)?.hash as Any);
            } else {
                print("Failed to upload image from cache");
            }
            DispatchQueue.main.async {
                self.mediasLoader.stopAnimating()
            }
        }
    }

    @IBAction func addMediaFromCamera() {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            return;
        }
        let picker = UIImagePickerController()
        picker.sourceType = .camera;
        picker.allowsEditing = true;
        picker.delegate = self;
        self.present(picker, animated: true);
    }


    @IBAction func uploadRandomImage(_ sender: Any) {
        // Alert confirm user wants to upload a random image
        let alert = UIAlertController(title: NSLocalizedString("medias.upload.random.alert.title", comment: ""), message: NSLocalizedString("medias.upload.random.alert.message", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: { action in
            self.uploadRandomImage();
        }))
        present(alert, animated: true)
    }

    func uploadRandomImage() {
        mediasLoader.startAnimating()
        if Reachability.isConnectedToNetwork() {
            print("Uploading random image");
            RandomMediaWebService.shared.getSquareRandomMedia { media in
                print("Got square media");

                guard let media = media else {
                    print("Error: media is nil");
                    return;
                }
                guard let imageData = try? Data(contentsOf: media.linkURL),
                      let mediaUIImage = UIImage(data: imageData)
                else {
                    print("Error: imageData is nil or mediaUIImage is nil");
                    return;
                }

                self.uploadMedia(fileName: media.name, image: mediaUIImage);
            }
        } else {
            self.mediasLoader.stopAnimating()
            // Alert no connection available, user can only upload medias to cache storage
            let alert = UIAlertController(title: NSLocalizedString("no.internet.title", comment: ""), message: NSLocalizedString("no.internet.functionalities.available", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: nil))
            present(alert, animated: true)
        }
    }
}

extension GalleryViewController: MediaDetailsViewDelegate {

    func reloadUserMedias() {
        self.retrieveUserMedias();
    }

    func removeMedia(media: Media) {
        DispatchQueue.main.sync {
            self.mediasLoader.startAnimating();
            self.medias.removeAll(where: { $0.name == media.name });
            self.mediasLoader.stopAnimating();
        }
    }

}
