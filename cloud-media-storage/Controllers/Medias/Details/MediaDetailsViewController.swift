//
//  EditMediaViewController.swift
//  cloud-media-storage
//
//  Created by William Quach on 22/07/2022.
//

import UIKit

protocol MediaDetailsViewDelegate: NSObjectProtocol {
    func reloadUserMedias();
    func removeMedia(media: Media);
}

class MediaDetailsViewController: UIViewController {

    weak var delegate: MediaDetailsViewDelegate?

    var hasBeenChanged = false;
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageViewLoader: UIActivityIndicatorView!

    var media: Media!;
    var mediaUIImage: UIImage!;

    static func newInstance(media: Media, mediaUIImage: UIImage) -> MediaDetailsViewController {
        let vc = MediaDetailsViewController()
        vc.media = media;
        vc.mediaUIImage = mediaUIImage;
        return vc;
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("media.details.page.title", comment: "");
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // If original image is different from the imageToFilter, then we need to update the media.
        if self.isMovingFromParent {
            if hasBeenChanged {
                self.delegate?.reloadUserMedias();
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        imageViewLoader.startAnimating();
        DispatchQueue.main.async {
            print("RETRIEVING MEDIA... \(self.media.name) (url : \(self.media.link))")
            self.updateImageView();
        }
    }

    func updateImageView() {
        imageView.image = mediaUIImage
        imageViewLoader.stopAnimating();
    }

    @IBAction func deleteMediaButtonClicked(_ sender: Any) {
        // Ask confirmation to delete
        let alert = UIAlertController(title: NSLocalizedString("media.delete.confirmation.title", comment: ""), message: NSLocalizedString("media.delete.confirmation.message", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("media.delete.confirmation.cancel", comment: ""), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: NSLocalizedString("media.delete.confirmation.delete", comment: ""), style: .destructive, handler: { action in
            // Delete media
            self.deleteMedia()
        }))
        self.present(alert, animated: true)
    }

    func deleteMedia() {
        // Delete media
        print("DELETING MEDIA... \(self.media.id)")
        DispatchQueue.main.async {
            self.imageViewLoader.startAnimating();
            print("START ANIMATING IN deleteMedia")

            MediaWebService.shared.deleteMedia(media: self.media) { response in
                print("BEFORE self.delegate?.removeMedia")

                self.delegate?.removeMedia(media: self.media);
                
                print("AFTER self.delegate?.removeMedia")
                
                DispatchQueue.main.async {
                    self.imageViewLoader.stopAnimating();
                    print("STOP ANIMATING IN deleteMedia")
                    self.navigationController?.popViewController(animated: true)
                }
            }

        }
    }

    @IBAction func editMediaButtonClicked(_ sender: Any) {
        let editMediaController = EditMediaViewController.newInstance(media: media, mediaUIImage: mediaUIImage)
        editMediaController.delegate = self;

        navigationController?.pushViewController(editMediaController, animated: true);
    }
}

extension MediaDetailsViewController: EditMediaViewDelegate {
    func updateCurrentMediaDetails(media: Media!, mediaUIImage: UIImage!, hasBeenChanged: Bool) {
        self.dismiss(animated: true) {
            self.mediaUIImage = mediaUIImage;
            self.media = media;
            self.hasBeenChanged = hasBeenChanged;
        }
    }
}
