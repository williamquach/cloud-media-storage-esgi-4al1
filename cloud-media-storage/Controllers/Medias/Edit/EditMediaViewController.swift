//
//  MediaDetailsViewController.swift
//  cloud-media-storage
//
//  Created by William Quach on 15/07/2022.
//

import UIKit

protocol EditMediaViewDelegate: NSObjectProtocol {
    func updateCurrentMediaDetails(media: Media!, mediaUIImage: UIImage!, hasBeenChanged: Bool);
}

class EditMediaViewController: UIViewController {
    weak var delegate: EditMediaViewDelegate?

    @IBOutlet weak var imageViewLoader: UIActivityIndicatorView!
    @IBOutlet weak var originalImage: UIImageView!
    @IBOutlet weak var imageToFilter: UIImageView!
    @IBOutlet weak var filtersScrollView: UIScrollView!

    var CIFilterNames = [
        "CIPhotoEffectChrome",
        "CIPhotoEffectFade",
        "CIPhotoEffectInstant",
        "CIPhotoEffectMono",
        "CIPhotoEffectNoir",
        "CIPhotoEffectProcess",
        "CIPhotoEffectTonal",
        "CIPhotoEffectTransfer",
        "CISepiaTone",
        "CIColorCrossPolynomial",
        "CIColorCube",
        "CIColorCubeWithColorSpace",
        "CIColorInvert",
        "CIColorMonochrome",
        "CIColorPosterize",
        "CIFalseColor",
        "CIMaskToAlpha",
        "CIMaximumComponent",
        "CIMinimumComponent",
        "CIVignette",
        "CIVignetteEffect",
    ]

    var media: Media!;
    var mediaUIImage: UIImage!;

    static func newInstance(media: Media, mediaUIImage: UIImage) -> EditMediaViewController {
        let vc = EditMediaViewController()
        vc.media = media;
        vc.mediaUIImage = mediaUIImage;
        return vc;
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("media.update.page.title", comment: "");
        imageViewLoader.startAnimating();
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("media.update.button.title", comment: ""), style: .plain, target: self, action: #selector(saveChangesOnMedia))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: NSLocalizedString("media.update.button.cancel", comment: ""), style: .plain, target: self, action: #selector(cancelChangesOnMedia))

        DispatchQueue.main.async {
            print("RETRIEVING MEDIA... \(self.media.name) (url : \(self.media.link))")
            self.originalImage.image = self.mediaUIImage
            self.imageToFilter.image = self.mediaUIImage
            self.setFiltersScrollView();
            self.imageViewLoader.stopAnimating();
        }
    }

    func setFiltersScrollView() {
        // Set filters buttons
        var xCoord: CGFloat = 5;
        let yCoord: CGFloat = 5;
        let buttonWidth: CGFloat = 70;
        let buttonHeight: CGFloat = 70;
        let gapBetweenButtons: CGFloat = 5;

        var itemCount = 0

        for i in 0..<CIFilterNames.count {
            itemCount = i

            // Button properties
            let filterButton = UIButton(type: .custom)
            filterButton.frame = CGRect(x: xCoord, y: yCoord, width: buttonWidth, height: buttonHeight);
            filterButton.tag = itemCount
            filterButton.addTarget(self, action: #selector(filterButtonTapped), for: .touchUpInside)
            filterButton.layer.cornerRadius = 6
            filterButton.clipsToBounds = true

            // Create filters for each button
            let ciContext = CIContext(options: nil)
            let coreImage = CIImage(image: originalImage.image!)
            let filter = CIFilter(name: "\(CIFilterNames[i])")
            filter!.setDefaults()
            filter!.setValue(coreImage, forKey: kCIInputImageKey)
            let filteredImageData = filter!.value(forKey: kCIOutputImageKey) as! CIImage
            let filteredImageRef = ciContext.createCGImage(filteredImageData, from: filteredImageData.extent)!
            let imageForButton = UIImage.init(cgImage: filteredImageRef);
            filterButton.setBackgroundImage(imageForButton, for: .normal)
            // Add Buttons in the Scroll View
            xCoord += buttonWidth + gapBetweenButtons
            filtersScrollView.addSubview(filterButton)
        } // END FOR LOOP

        // Resize Scroll View
        filtersScrollView.contentSize = CGSize(width: buttonWidth * CGFloat(itemCount + 2), height: yCoord)
    }

    @objc func filterButtonTapped(sender: UIButton) {
        let button = sender as UIButton
        imageToFilter.image = button.backgroundImage(for: UIControl.State.normal);
    }

    @objc func cancelChangesOnMedia(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true);
    }

    @objc func saveChangesOnMedia() {
        print("SAVING CHANGES ON MEDIA...")
        imageViewLoader.startAnimating();
        MediaWebService.shared.updateMedia(id: media.id, fileName: media.name, image: imageToFilter.image!, completion: { mediaUpdated in
            self.media = Media(id: self.media.id, name: self.media.name, link: mediaUpdated.url, linkURL: URL(string: mediaUpdated.url)!);
            print("MEDIA UPDATED : \(mediaUpdated)");
        })

        imageViewLoader.stopAnimating()
        showToast(message: NSLocalizedString("media.update.success.message", comment: ""), font: .systemFont(ofSize: 14));
        // Wait for the toast to be displayed before dismissing the view
        // Send data to previous view controller
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            let hasBeenChanged = self.originalImage.image != self.imageToFilter.image;
            self.delegate?.updateCurrentMediaDetails(media: self.media, mediaUIImage: self.imageToFilter.image, hasBeenChanged: hasBeenChanged);
            self.navigationController?.popViewController(animated: true);
        }
    }
}

