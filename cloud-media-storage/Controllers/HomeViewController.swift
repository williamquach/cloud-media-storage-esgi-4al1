//
//  HomeViewController.swift
//  cloud-media-storage
//
//  Created by William Quach on 02/07/2022.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var welcomeTitleLabel: UILabel!
    @IBOutlet weak var welcomeSubtitleLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var moreAboutButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad();
        self.navigationController?.navigationBar.tintColor = ColorUtils.getApplicationBasicColor();
        self.welcomeTitleLabel.text = NSLocalizedString("home.welcome.title", comment: "")
        self.welcomeSubtitleLabel.text = NSLocalizedString("home.welcome.subtitle", comment: "")
        self.continueButton.setTitle(NSLocalizedString("home.button.continue", comment: ""), for: .normal)
        self.moreAboutButton.setTitle(NSLocalizedString("home.button.more_about", comment: ""), for: .normal)
    }

    @IBAction func continueButtonClicked(_ sender: Any) {
        self.navigationController?.pushViewController(LoginViewController(), animated: true);
    }

    @IBAction func moreAboutButtonClicked(_ sender: Any) {
        self.navigationController?.pushViewController(MoreAboutViewController(), animated: true);
    }
}
