//
//  SignInViewController.swift
//  cloud-media-storage
//
//  Created by William Quach on 02/07/2022.
//

import UIKit

class SignUpViewController: UIViewController {
    @IBOutlet weak var registerLoading: UIActivityIndicatorView!

    @IBOutlet weak var registerTitle: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var nicknameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var goToLoginButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        self.title = NSLocalizedString("sign_up.page.title", comment: "")
        self.navigationItem.setHidesBackButton(true, animated: false);

        self.registerTitle.text = NSLocalizedString("sign_up.title", comment: "");
        self.nameTextField.placeholder = NSLocalizedString("sign_up.form.firstname.placeholder", comment: "");
        self.lastNameTextField.placeholder = NSLocalizedString("sign_up.form.lastname.placeholder", comment: "");
        self.nicknameTextField.placeholder = NSLocalizedString("sign_up.form.nickname.placeholder", comment: "");
        self.emailTextField.placeholder = NSLocalizedString("sign_up.form.email.placeholder", comment: "");
        self.passwordTextField.placeholder = NSLocalizedString("sign_up.form.password.placeholder", comment: "");
        self.registerButton.setTitle(NSLocalizedString("sign_up.form.button.register", comment: ""), for: .normal)
        self.goToLoginButton.setTitle(NSLocalizedString("login.page.title", comment: ""), for: .normal);
    }

    @IBAction func clickedOnRegister(_ sender: Any) {
        registerLoading.startAnimating();
        guard   let name = nameTextField.text,
                let lastName = lastNameTextField.text,
                let nickname = nicknameTextField.text,
                let email = emailTextField.text,
                let password = passwordTextField.text,
                name != "",
                lastName != "",
                nickname != "",
                email != "",
                password != "",
                password.count > 3
        else {
            registerLoading.stopAnimating()
            let alert = UIAlertController(title: NSLocalizedString("sign_up.incorrect.register.title", comment: ""), message: NSLocalizedString("sign_up.incorrect.register.message", comment: ""), preferredStyle: .alert);
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel));
            self.present(alert, animated: true)
            return;
        }

        UserWebService.shared.register(nickname: nickname, password: password, email: email, firstname: name, lastname: lastName) { userRegisterResponse in
            DispatchQueue.main.async {
                self.registerLoading.stopAnimating()
            }
            if userRegisterResponse.success {
                DispatchQueue.main.sync {
                    self.navigationController?.pushViewController(LoginViewController(), animated: true);
                }
            } else {
                let alert = UIAlertController(title: NSLocalizedString("sign_up.incorrect.register.title", comment: ""), message: userRegisterResponse.message, preferredStyle: .alert);
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel));
                self.present(alert, animated: true)
            }
        }
    }

    @IBAction func clickedOnLoginButton(_ sender: Any) {
        navigationController?.pushViewController(LoginViewController(), animated: true);
    }
}
