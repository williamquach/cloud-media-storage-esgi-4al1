//
//  LoginViewController.swift
//  cloud-media-storage
//
//  Created by William Quach on 02/07/2022.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var appLogo: UIImageView!
    var clickedOnLogo = 0;

    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var loginTitleLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginSubmitButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad();
        self.hideKeyboardWhenTappedAround()

        self.navigationController?.navigationBar.tintColor = ColorUtils.getApplicationBasicColor();
        self.navigationItem.setHidesBackButton(true, animated: false);

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.onLogoClicked))
        appLogo.isUserInteractionEnabled = true
        appLogo.addGestureRecognizer(tapGestureRecognizer)

        self.loginTitleLabel.text = NSLocalizedString("login.title", comment: "");
        self.emailTextField.placeholder = NSLocalizedString("login.form.mail.placeholder", comment: "");
        self.passwordTextField.placeholder = NSLocalizedString("login.form.password.placeholder", comment: "");
        self.loginSubmitButton.setTitle(NSLocalizedString("login.form.submit", comment: ""), for: .normal);
        self.forgotPasswordButton.setTitle(NSLocalizedString("login.form.forgot_password", comment: ""), for: .normal);
        self.signUpButton.setTitle(NSLocalizedString("sign_up.page.title", comment: ""), for: .normal);
    }

    @objc func onLogoClicked() {
        clickedOnLogo += 1;
        if clickedOnLogo % 10 == 0 {
            self.navigationController?.pushViewController(GalleryViewController(), animated: true);
        }
    }

    @IBAction func loginButtonClicked(_ sender: Any) {
        loader.startAnimating()
        // TODO: AUTHENTICATE
        guard   self.emailTextField.text != "",
                self.passwordTextField.text != "",
                let nickname = self.emailTextField.text,
                let password = self.passwordTextField.text,
                nickname != "",
                password != ""
        else {
            let alert = UIAlertController(title: NSLocalizedString("login.incorrect.login.title", comment: ""), message: NSLocalizedString("login.incorrect.login.message", comment: ""), preferredStyle: .alert);
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .cancel));
            self.present(alert, animated: true)
            self.loader.stopAnimating()
            return;
        }

        UserWebService.shared.login(nickname: nickname, password: password) { status in
            print("STATUS WE GOT \(status)")

            DispatchQueue.main.sync {
                self.loader.stopAnimating()
                if status.success == true {
                    print("STATUS SUCCESS")
                    self.navigationController?.pushViewController(GalleryViewController(), animated: true);
                    return;
                } else {
                    print("STATUS FAILED")
                    let alert = UIAlertController(title: NSLocalizedString("login.incorrect.login.title", comment: ""), message: status.message, preferredStyle: .alert);
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel));
                    self.present(alert, animated: true)
                }
            }
        }
    }

    @IBAction func finishedEnterNickname(_ sender: Any) {
        passwordTextField.becomeFirstResponder();
    }

    @IBAction func passwordDone(_ sender: Any) {
        loginButtonClicked(sender);
    }

    @IBAction func forgotPasswordButtonClicked(_ sender: Any) {
        self.navigationController?.pushViewController(ForgotPasswordViewController(), animated: true);
    }

    @IBAction func signUpButtonClicked(_ sender: Any) {
        self.navigationController?.pushViewController(SignUpViewController(), animated: true);
    }
}
