//
//  ForgotPasswordViewController.swift
//  cloud-media-storage
//
//  Created by William Quach on 02/07/2022.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    @IBOutlet weak var pageNameTextField: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var buttonSendForgotPasswordButton: UIButton!
    @IBOutlet weak var goToLoginButton: UIButton!
    @IBOutlet weak var goToSignUpButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        self.navigationController?.navigationBar.tintColor = ColorUtils.getApplicationBasicColor();
        self.navigationItem.setHidesBackButton(true, animated: false);

        self.title = NSLocalizedString("forgot_password.page.title", comment: "");

        self.pageNameTextField.text = NSLocalizedString("forgot_password.page.title", comment: "");

        self.emailTextField.placeholder = NSLocalizedString("forgot_password.form.email", comment: "")
        self.buttonSendForgotPasswordButton.setTitle(NSLocalizedString("forgot_password.button.forgot", comment: ""), for: .normal);

        self.goToLoginButton.setTitle(NSLocalizedString("login.page.title", comment: ""), for: .normal)
        self.goToSignUpButton.setTitle(NSLocalizedString("sign_up.page.title", comment: ""), for: .normal);
    }

    @IBAction func forgotPasswordButton(_ sender: Any) {
        let alert = UIAlertController(title: "Wait wait...", message: "NOT IMPLEMENTED !", preferredStyle: .alert);
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel));
        self.present(alert, animated: true)
    }

    @IBAction func goToLoginButtonClicked(_ sender: Any) {
        self.navigationController?.pushViewController(LoginViewController(), animated: true)
    }

    @IBAction func goToSignUpButtonClicked(_ sender: Any) {
        self.navigationController?.pushViewController(SignUpViewController(), animated: true)
    }
}
