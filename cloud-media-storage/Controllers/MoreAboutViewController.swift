//
//  MoreAboutViewController.swift
//  cloud-media-storage
//
//  Created by William Quach on 02/07/2022.
//

import UIKit

class MoreAboutViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("more_about.page.title", comment: "");
    }
}
