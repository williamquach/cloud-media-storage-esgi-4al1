//
//  AppDelegate.swift
//  cloud-media-storage
//
//  Created by William Quach on 02/07/2022.
//

import UIKit
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?;

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let w = UIWindow(frame: UIScreen.main.bounds);
        
        w.rootViewController = UINavigationController(rootViewController: HomeViewController()); // Intégration de la home en tant que controlleur principal de l'application
        w.makeKeyAndVisible();
        self.window = w; // Mémorisation de la fenêtre pour éviter de la détruire à la fin de la fonction
        
        w.rootViewController?.navigationController?.navigationBar.tintColor = UIColor.brown
        
        return true
    }
}

