//
//  ColorUtils.swift
//  cloud-media-storage
//
//  Created by William Quach on 02/07/2022.
//

import Foundation
import UIKit

class ColorUtils {
    
    static let RGB_RANGE: Float = 255;
    static let APP_MAIN_RED_RGB_COLOR: Float = 225;
    static let APP_MAIN_GREEN_RGB_COLOR: Float = 125;
    static let APP_MAIN_BLUE_RGB_COLOR: Float = 120;

    static let APP_MAIN_RGB_COLOR = UIColor(
        red: CGFloat(APP_MAIN_RED_RGB_COLOR / RGB_RANGE),
        green: CGFloat(APP_MAIN_GREEN_RGB_COLOR / RGB_RANGE),
        blue: CGFloat(APP_MAIN_BLUE_RGB_COLOR / RGB_RANGE),
        alpha: 1
    )
    
    static func getApplicationBasicColor() -> UIColor {
        return APP_MAIN_RGB_COLOR;
    }
    
}
